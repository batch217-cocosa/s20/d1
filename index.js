// While loop


let count = 5;

while (count !== 0){

	console.log("While: "+ count);

	count--;
}



// Do-While loop


/*let number = Number(prompt("Give me a number"));


do{

	console.log( "Do While: " + number);


		// Increases the value of number until ti reaches 10

		number+= 1;
		//number = number +1;`
	} while (number <=10);*/



	// For Loops


	/*

	a for loop is more flexible than while and do-while loops. It consists of three parts:

		1. The "initialization" value that will track the progression of the loop.

		2. The "expression/condition" that ewill be evaluated which will determine whether the loop will run one more time.

		3. The "finalExpression" inidicates how to advance the loop.


		-Syntax 

		for(initialization; expression/condition; finalExpression){
			statement
		}



	*/



/*	for (let count = 0; count<= 20; count++){
		console.log(count);
	}

		for (let count = 0; count<= 12; count++){
		console.log(count);
	}*/



	let myString = "Alex";
	console.log (myString.length);

	console.log(myString[0]);


for( x = 0; x < myString.length; x++ ){
	console.log(myString[x]);
}


let myName = "JhEmMo";

for(let i = 0; i < myName.length; i++){

	if(

		myName[i].toLowerCase() == "a" || 
		myName[i].toLowerCase() == "e" || 
		myName[i].toLowerCase() == "i" || 
		myName[i].toLowerCase() == "o" || 
		myName[i].toLowerCase() == "u" 
		
		){

		// if the letter is a vowel
		console.log(3);
	}
	else{
		console.log(myName[i].toLowerCase());
	}


}


// for-ifcontinue and for-ifbreak

for (let count = 0; count  <=  20; count++){
	if (count % 2 === 0){
		continue;
	}
	console.log("Continue and break: " + count);
	if (count > 10){
		break;
	}
}


let name = "alexandro";

for (let i = 0; name.length; i++){
	console.log(name[i]);

	if (name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if (name[i] == "d"){
		break;
	}
}
